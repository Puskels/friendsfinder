//
//  MapViewSnapshotTest.swift
//  FriendsFinderTests
//
//  Created by McSims on 27/11/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import XCTest
import FBSnapshotTestCase
@testable import FriendsFinder

/// This tests intend to check visual appearance of map view.
/// MKMapView view is fragile and requires some tolerance in comparison.
class MapViewSnapshotTest: FBSnapshotTestCase {

    let window = UIWindow()

    override func setUp() {
        super.setUp()
        self.recordMode = false
    }

    func testMapViewHasFinishedRendering() {
        let expectation = XCTestExpectation(description: "Waiting for map to load!")
        let streamMock = StreamProducerMock(streamString: "")
        let viewModel = MapViewModel(withStremer: streamMock)
        let vc = MapViewController(withViewModel: viewModel)
        vc.onMapRenderCompletion = {
            self.FBSnapshotVerifyView(vc.view, overallTolerance: 2.0)
            expectation.fulfill()
        }
        window.rootViewController = vc
        window.makeKeyAndVisible()
        wait(for: [expectation], timeout: 10.0)
    }

    func testMapViewHasReceivedLocation() {
        let expectation = XCTestExpectation(description: "Waiting for map to load and draw location!")
        let streamMock = StreamProducerMock(streamString: TCPResponses.messageOneUserString)
        let viewModel = MapViewModel(withStremer: streamMock)
        let vc = MapViewController(withViewModel: viewModel)

        window.rootViewController = vc
        window.makeKeyAndVisible()

        streamMock.pushNextEvent()

        XCTAssertTrue(vc.mapView?.annotations.count == 1)
        vc.mapView?.selectAnnotation(vc.mapView!.annotations.first!, animated: false)

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            let view = vc.view
            self.FBSnapshotVerifyView(view!, overallTolerance: 2.0)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 4.0)
    }
}
