//
//  GeoCoderTests.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import XCTest
import MapKit

class GeoCoderTest: XCTestCase {

    func testCorrectAddress() {
        let expectation = XCTestExpectation(description: "Waiting for street decode!")
        let location = CLLocation(latitude: 56.9495677035, longitude: 24.1064071655)
        let coder = GeoCoder()
        coder.address(for: location) { street in
            XCTAssertTrue(street == "Zirgu iela 17–33")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 3.0)

    }
}
