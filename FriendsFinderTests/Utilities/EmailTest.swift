//
//  EmailTest.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import XCTest

class EmailTest: XCTestCase {

    func testEmailRegex() {
        XCTAssertFalse("".isValidEmail)
        XCTAssertFalse("a".isValidEmail)
        XCTAssertFalse("a@".isValidEmail)
        XCTAssertFalse("a@.".isValidEmail)
        XCTAssertFalse("a@.c".isValidEmail)
        XCTAssertFalse("a@.co".isValidEmail)
        XCTAssertTrue("a@a.co".isValidEmail)
    }
}
