//
//  CommandsFactoryTest.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import XCTest
@testable import FriendsFinder

class CommandsFactoryTest: XCTestCase {

    func testBuider() {
        let message = "COMMAND_MESSAGE"
        let delimeter = "DELIMETER"
        let command = CommandBuilder.Commands.authorize
        let commandString = CommandBuilder(command: command, delimeter: delimeter).append(string: message).build()
        XCTAssertTrue(commandString == "\(command.rawValue.uppercased()) \(delimeter)\(message)")
    }
}
