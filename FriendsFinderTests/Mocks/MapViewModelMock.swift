//
//  MapViewModelMock.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

final class MapViewModelMock: MapViewModelProtocol {
    var onZoomToRegion: ((User) -> Void)?
    var onUserReceived: ((User) -> Void)?
    var onUserLocationUpdate: ((UserLocation) -> Void)?
}
