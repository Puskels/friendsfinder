//
//  StreamProducerMock.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

class StreamProducerMock: StreamProducable {

    var onMessageReceived: ((String) -> Void)?

    private let nextMessage: String

    init(streamString: String) {
        nextMessage = streamString
    }

    func pushNextEvent() {
        onMessageReceived?(nextMessage)
    }
}
