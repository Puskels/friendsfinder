//
//  CommandsParserTest.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import XCTest

class CommandsParserTest: XCTestCase {

    func testExtractCorrectCommandAndMessage() {
        let result = CommandParser.extractCommand(from: TCPResponses.messageOneUserString)
        XCTAssertTrue(result?.command == CommandBuilder.Commands.userList)
        XCTAssertTrue(result?.remainingMessage == TCPResponses.oneUserValues)
    }

    func testNoContentHasCorrectCommand() {
        let result = CommandParser.extractCommand(from: TCPResponses.userListType)
        XCTAssertTrue(result?.command == CommandBuilder.Commands.userList)
    }

    func testNoContentButSpaceHasNoMessage() {
        let result = CommandParser.extractCommand(from: "\(TCPResponses.userListType) ")
        XCTAssertTrue(result?.command == CommandBuilder.Commands.userList)
    }

    func testCommandParse() {
        let dummyContent = "DUMMY_VALUES"
        let userList = "\(CommandBuilder.Commands.userList.rawValue) \(dummyContent)"
        XCTAssertTrue(CommandParser.parseType(from: userList) == CommandBuilder.Commands.userList)
        let authorize = "\(CommandBuilder.Commands.authorize.rawValue) \(dummyContent)"
        XCTAssertTrue(CommandParser.parseType(from: authorize) == CommandBuilder.Commands.authorize)
        let update = "\(CommandBuilder.Commands.update.rawValue) \(dummyContent)"
        XCTAssertTrue(CommandParser.parseType(from: update) == CommandBuilder.Commands.update)
        let unknown = "\("UNKNOWN_COMMAND") \(dummyContent)"
        XCTAssertNil(CommandParser.parseType(from: unknown))
    }

    func testEnumConstructor() {
        let userList = CommandBuilder.Commands.userList.rawValue
        XCTAssertTrue(CommandParser.command(from: userList) == CommandBuilder.Commands.userList)
        let authorize = CommandBuilder.Commands.authorize.rawValue
        XCTAssertTrue(CommandParser.command(from: authorize) == CommandBuilder.Commands.authorize)
        let update = CommandBuilder.Commands.update.rawValue
        XCTAssertTrue(CommandParser.command(from: update) == CommandBuilder.Commands.update)
        let unknown = "UNKNOWN_COMMAND"
        XCTAssertNil(CommandParser.command(from: unknown))
    }

    func testTypeParsePerformane() {
        self.measure {
            _ = CommandParser.parseType(from: CommandBuilder.Commands.userList.rawValue)
        }
    }

    func testExtractCommandPerformane() {
        self.measure {
            _ = CommandParser.extractCommand(from: TCPResponses.messageOneUserString)
        }
    }
}
