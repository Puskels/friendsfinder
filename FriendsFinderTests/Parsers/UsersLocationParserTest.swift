//
//  UsersLocationParser.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import XCTest

class UsersLocationParserTest: XCTestCase {

    func testOneUserLocationMessageSuccessParser() {
        let locations = UsersLocationParser.parseUserLocation(TCPResponses.userLocationValues)
        let expectedLocations = [UserLocation(userId: "101", latitude: 56.9495677035, longitude: 24.1064071655)]
        XCTAssertTrue(expectedLocations == locations)
    }

    func testParserPerformance() {
        self.measure {
            _ = UsersLocationParser.parseUserLocation(TCPResponses.userLocationValues)
        }
    }
}
