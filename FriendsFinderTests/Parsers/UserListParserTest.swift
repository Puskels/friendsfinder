//
//  MessageParserTest.swift
//  FriendsFinderTests
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import XCTest

class UserListParserTest: XCTestCase {

    func testOneUserListMessageSuccessParser() {
        let users = UsersListParser.parseUserList(TCPResponses.oneUserValues)
        let expectedUser = User(id: "101",
                                name: "Jānis Bērziņš",
                                avatar: URL(string: "https://i4.ifrype.com/profile/000/324/v1559116100/ngm_324.jpg"),
                                latitude: 56.9495677035,
                                longitude: 24.1064071655)
        XCTAssertTrue([expectedUser] == users)
    }

    func testCoupleUsersListMessageSuccessParser() {
        let users = UsersListParser.parseUserList(TCPResponses.messageTwoUsersString)
        XCTAssertTrue(users?.count == 2)
    }

    func testParserPerformance() {
        self.measure {
            _ = UsersListParser.parseUserList(TCPResponses.messageALotOfUsersString)
        }
    }
}
