//
//  MapViewModelProtocol.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

protocol MapViewModelProtocol {
    var onZoomToRegion: ((User)->Void)? { get set }
    var onUserReceived: ((User)->Void)? { get set }
    var onUserLocationUpdate: ((UserLocation)->Void)? { get set }
}
