//
//  MapVC.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation
import MapKit

final class MapViewController: UIViewController {

    var onMapLoadCompletion: (() -> Void)?
    var onMapRenderCompletion: (() -> Void)?

    var mapView: MKMapView?

    private var viewModel: MapViewModelProtocol!

    init(withViewModel viewModel: MapViewModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupMap()
        addViewModelHandlers()
    }

    private func addViewModelHandlers() {
        viewModel.onUserReceived = {[weak self] user in
            let annotation = Annotation(user)
            self?.mapView?.addAnnotation(annotation)
        }

        viewModel.onUserLocationUpdate = {[weak self] userLocation in
            self?.mapView?.annotations
                .compactMap { $0 as? Annotation }
                .filter({ $0.user.id == userLocation.userId })
                .forEach { existingMarker in
                    let coordinate = CLLocationCoordinate2D(latitude: userLocation.latitude,
                                                            longitude: userLocation.longitude)
                    existingMarker.update(coordinate: coordinate)
                }
        }

        viewModel.onZoomToRegion = {[weak self] user in
            self?.zoomToLastLocationRegion(user)
        }
    }

    private func zoomToLastLocationRegion(_ location: User) {
        let viewRegion = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: location.latitude,
                                                                           longitude: location.longitude),
                                            latitudinalMeters: 200,
                                            longitudinalMeters: 200)
        mapView?.setRegion(viewRegion, animated: true)
    }
}
// - MARK: -
// - MARK: Views setup and layout -
extension MapViewController {
    private func setupMap() {
        let mapView = MKMapView(frame: view.frame)
        mapView.delegate = self
        layout(mapView)
        self.mapView = mapView
    }

    private func layout(_ mapView: MKMapView) {
        view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        let leading = mapView.equalConstraint(to: NSLayoutConstraint.Attribute.leading, with: view)
        let top = mapView.equalConstraint(to: NSLayoutConstraint.Attribute.top, with: view)
        let trailing = mapView.equalConstraint(to: NSLayoutConstraint.Attribute.trailing, with: view)
        let bottom = mapView.equalConstraint(to: NSLayoutConstraint.Attribute.bottom, with: view)
        NSLayoutConstraint.activate([trailing, leading, top, bottom])
    }
}
// - MARK: -
// - MARK: MKMapViewDelegate -
extension MapViewController: MKMapViewDelegate {
    func mapViewDidFinishLoadingMap(_ mapView: MKMapView) {
        onMapLoadCompletion?()
    }

    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {
        onMapRenderCompletion?()
    }

    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let userAnnotation = annotation as? Annotation else { return nil }
        guard let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: userAnnotation.reuseIdentifier) else {
            return AnnotationView(userAnnotation: userAnnotation, reuseIdentifier: userAnnotation.reuseIdentifier)
        }
        return annotationView
    }
}
