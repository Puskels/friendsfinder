//
//  MapVM.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

final class MapViewModel: MapViewModelProtocol {

    var onZoomToRegion: ((User)->Void)?
    var onUserReceived: ((User)->Void)?
    var onUserLocationUpdate: ((UserLocation)->Void)?

    private var tcpClient: StreamProducable
    
    init(withStremer streamer: StreamProducable) {
        tcpClient = streamer
        addNetworkHandlers()
    }

    private func addNetworkHandlers() {
        tcpClient.onMessageReceived = {[weak self] message in
            guard let result = CommandParser.extractCommand(from: message) else {
                return
            }
            switch result.command {
            case .userList:
                let users = UsersListParser.parseUserList(result.remainingMessage)
                users?.forEach({ self?.onUserReceived?($0) })
                if let user = users?.last {
                    self?.onZoomToRegion?(user)
                }
            case .update:
                UsersLocationParser
                    .parseUserLocation(result.remainingMessage)?
                    .forEach({ self?.onUserLocationUpdate?($0) })
            default:
                print("Unhandled command: \(result.command)")
            }
        }
    }
}
