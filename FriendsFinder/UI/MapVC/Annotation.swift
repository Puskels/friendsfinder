//
//  UserAnnotation.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import MapKit
import Contacts

final class Annotation: MKPointAnnotation {

    private let geoCoder = GeoCoder()

    var user: User!
    var reuseIdentifier: String {
        return "User-\(user.id)"
    }

    init(_ user: User) {
        super.init()
        self.user = user
        title = user.name
        update(coordinate: user.coordinate2D)
    }

    func update(coordinate: CLLocationCoordinate2D) {
        subtitle = nil
        UIView.animate(withDuration: 0.5, animations: {
            self.coordinate = coordinate
        }, completion: { success in
            guard !success else { return }
            self.coordinate = coordinate
        })
        updateAddressInfo(for: coordinate)
    }

    private func updateAddressInfo(for coordinate: CLLocationCoordinate2D) {
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        geoCoder.address(for: location) {[weak self] street in
            self?.subtitle = street
        }
    }
}
