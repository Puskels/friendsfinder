//
//  UserAnnotationView.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import UIKit
import MapKit
import SDWebImage

final class AnnotationView: MKAnnotationView {

    private var pinView: UIImageView!
    private var avatarImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))

    var user: User!

    override func prepareForDisplay() {
        super.prepareForDisplay()
        avatarImageView.sd_setImage(with: user.avatar)
    }

    init(userAnnotation: Annotation, reuseIdentifier: String?) {
        super.init(annotation: userAnnotation, reuseIdentifier: reuseIdentifier)
        user = userAnnotation.user

        canShowCallout = true
        image = UIImage(named: "location-pin", in: Bundle(for: AnnotationView.self), compatibleWith: nil)
        frame = CGRect(x: 0, y: 0, width: 15, height: 15)

        leftCalloutAccessoryView = avatarImageView
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
