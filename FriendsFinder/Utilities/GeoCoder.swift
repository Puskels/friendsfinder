//
//  GeoCoder.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import MapKit
import Contacts

struct GeoCoder {

    private let geocoder = CLGeocoder()

    func address(for location: CLLocation, result: @escaping ((String) -> Void)) {
        geocoder.reverseGeocodeLocation(location, completionHandler: {(placemarks, error) in
            guard error == nil, let placemark = placemarks?.first, let street = placemark.postalAddress?.street else {
                return
            }
            result(street)
        })
    }
}
