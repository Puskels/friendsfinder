//
//  MKMapView+Constraits.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import UIKit
import MapKit

extension MKMapView {
    func equalConstraint(to attribute: NSLayoutConstraint.Attribute, with view: UIView) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self,
                                  attribute: attribute,
                                  relatedBy: NSLayoutConstraint.Relation.equal,
                                  toItem: view,
                                  attribute: attribute,
                                  multiplier: 1,
                                  constant: 0)
    }
}
