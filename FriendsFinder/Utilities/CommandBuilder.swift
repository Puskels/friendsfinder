//
//  CommandsFactory.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

final class CommandBuilder {

    enum Commands: String {
        case authorize
        case userList = "userlist"
        case update
    }

    let command: Commands
    let delimeter: String

    private var resultString = ""

    public init(command: Commands, delimeter: String) {
        self.command = command
        self.delimeter = delimeter
    }

    func append(string: String) -> CommandBuilder {
        resultString = "\(command.rawValue.uppercased()) " + delimeter + string
        return self
    }

    func build() -> String {
        return resultString
    }
}
