//
//  UIViewController+Alert.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import UIKit

typealias EmailAlertCompletion = ((String) -> Void)

protocol EmailAlertPresentable {
    func showEmailAlert(title: String?, message: String, completion: @escaping EmailAlertCompletion)
}

extension EmailAlertPresentable where Self: UIViewController {
    func showEmailAlert(title: String?, message: String, completion: @escaping EmailAlertCompletion) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "email@email.com"
        }
        let action = UIAlertAction(title: "Lets go!", style: .default, handler: { [weak alert] (_) in
            guard let textField = alert?.textFields?.first, let text = textField.text else { return }
            completion(text)
        })
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
    }
}
