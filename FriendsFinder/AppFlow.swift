//
//  AppFlow.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import UIKit

// In context of the project email prompt is shown only on MapViewController from AppFlow
// so it is good to provide comformance to protocol here, where it is used.
extension MapViewController: EmailAlertPresentable {}

final class AppFlow {

    private lazy var streamer = StreamClient(with: StreamConfig(host: "ios-test.printful.lv", port: 6111))
    private lazy var viewModel = MapViewModel(withStremer: streamer)
    private lazy var mapViewController = MapViewController(withViewModel: viewModel)

    private let window: UIWindow

    init(rootWindow: UIWindow) {
        self.window = rootWindow
    }

    func startAppFlow() {
        displayMap()
        showEmailInputAlert(title: "Welcome!", message: "Please enter your email")
    }

    private func displayMap() {
        window.rootViewController = mapViewController
        window.makeKeyAndVisible()
    }
}
// - MARK: -
// - MARK: Email input logic -
extension AppFlow {
    private func showEmailInputAlert(title: String, message: String) {
        mapViewController.showEmailAlert(title: title, message: message) {[weak self] email in
            self?.handleResponse(email)
        }
    }

    private func handleResponse(_ email: String) {
        guard email.isValidEmail else {
            showEmailInputAlert(title: "Ooops!",
                                message: "Looks like email you have entered is not in valid format. Please try again.")
            return
        }
        startLocationTrack(with: email)
    }

    private func startLocationTrack(with email: String) {
        let commandString = CommandBuilder(command: .authorize, delimeter: "\n")
                .append(string: email)
                .build()
        streamer.send(message: commandString)
    }
}
