//
//  UserLocationUpdate.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

struct UserLocation: Hashable {
    let userId: String
    let latitude: Double
    let longitude: Double
}
