//
//  Message.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation
import MapKit

struct User: Hashable {
    let id: String
    let name: String
    let avatar: URL?
    let latitude: Double
    let longitude: Double

    var coordinate2D: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude,
                                      longitude: longitude)
    }
}
