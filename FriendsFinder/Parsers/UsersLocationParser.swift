//
//  UsersLocationParser.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

protocol UserLocationParserProtocol {
    static func parseUserLocation(_ message: String) -> [UserLocation]?
}

struct UsersLocationParser: UserLocationParserProtocol {

    static func parseUserLocation(_ message: String) -> [UserLocation]? {
        let locationsArray = message.components(separatedBy: "\n")
        return locationsArray.compactMap({ valuesString -> UserLocation? in
            let userValues = valuesString.components(separatedBy: ",")
            guard userValues.count == 3,
                let lat = Double(userValues[1]),
                let lon = Double(userValues[2]) else {
                    return nil
            }
            let userId = userValues[0]
            return UserLocation(userId: userId, latitude: lat, longitude: lon)
        })
    }
}
