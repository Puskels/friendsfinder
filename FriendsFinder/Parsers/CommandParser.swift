//
//  CommandParser.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

struct CommandParser {

    static func extractCommand(from message: String) -> (command: CommandBuilder.Commands, remainingMessage: String)? {
        guard let command = parseType(from: message) else { return nil }
        let pureValues = message.replacingOccurrences(of: "\(command.rawValue.uppercased()) ", with: "")
        return (command, pureValues)
    }

    static func parseType(from message: String) -> CommandBuilder.Commands? {
        guard let firstComponent = message.components(separatedBy: " ").first else {
            return nil
        }
        return command(from: firstComponent)
    }

    static func command(from string: String) -> CommandBuilder.Commands? {
        return CommandBuilder.Commands(rawValue: string.lowercased())
    }
}
