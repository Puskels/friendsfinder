//
//  UsersListParser.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

protocol UserListParserProtocol {
    static func parseUserList(_ message: String) -> [User]?
}

struct UsersListParser: UserListParserProtocol {

    static func parseUserList(_ message: String) -> [User]? {
        let userValuesArray = message.components(separatedBy: ";")
        return userValuesArray.compactMap({ valuesString -> User? in
            let userValues = valuesString.components(separatedBy: ",")
            guard userValues.count == 5,
                let lat = Double(userValues[3]),
                let lon = Double(userValues[4]) else {
                return nil
            }
            let userId = userValues[0]
            let name = userValues[1]
            let avatarUrlString = userValues[2]
            return User(id: userId, name: name, avatar: URL(string: avatarUrlString), latitude: lat, longitude: lon)
        })
    }
}
