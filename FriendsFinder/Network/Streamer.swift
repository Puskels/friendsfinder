//
//  Streamer.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import UIKit

typealias Streamer = StreamProducable & StreamAcceptable

final class StreamClient: NSObject, Streamer {

    var onMessageReceived: ((String) -> Void)?

    private var inputStream: InputStream!
    private var outputStream: OutputStream!

    private let maxReadLength = 4096

    init(with configuration: StreamConfig) {
        super.init()
        openConnection(to: configuration)
    }

    private func openConnection(to config: StreamConfig) {
        var readStream: Unmanaged<CFReadStream>?
        var writeStream: Unmanaged<CFWriteStream>?

        CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, config.host as CFString, config.port, &readStream, &writeStream)

        inputStream = readStream!.takeRetainedValue()
        outputStream = writeStream!.takeRetainedValue()

        inputStream.delegate = self

        inputStream.schedule(in: .current, forMode: .common)
        outputStream.schedule(in: .current, forMode: .common)

        inputStream.open()
        outputStream.open()
    }

    func send(message: String) {
        let data = message.data(using: .utf8)!
        _ = data.withUnsafeBytes {
            guard let pointer = $0.baseAddress?.assumingMemoryBound(to: UInt8.self) else {
                return
            }
            outputStream.write(pointer, maxLength: data.count)
        }
    }

    func closeConnection() {
        inputStream.close()
        outputStream.close()
    }
}

extension StreamClient: StreamDelegate {

    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        switch eventCode {
        case .hasBytesAvailable:
            readAvailableBytes(stream: aStream as! InputStream)
        case .endEncountered:
            closeConnection()
        case .errorOccurred:
            print("ERROR OCURRED: \(aStream.streamError?.localizedDescription ?? "")")
        default:
            print("UNUSED EVENT RECEIVED: \(eventCode)")
        }
    }

    private func readAvailableBytes(stream: InputStream) {
        let buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: maxReadLength)
        while stream.hasBytesAvailable {
            let numberOfBytesRead = inputStream.read(buffer, maxLength: maxReadLength)
            if numberOfBytesRead < 0, let error = stream.streamError {
                print(error)
                break
            }
            guard let message = String(bytesNoCopy: buffer, length: numberOfBytesRead, encoding: .utf8, freeWhenDone: true) else {
                return
            }
            debugPrint("👨🏽‍💻 👨🏽‍💻 👨🏽‍💻 Message received: \(message)")
            onMessageReceived?(message)
        }
    }
}
