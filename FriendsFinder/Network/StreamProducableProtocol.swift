//
//  StreamProducableProtocol.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

protocol StreamProducable {
    var onMessageReceived: ((String) -> Void)? { get set }
}
