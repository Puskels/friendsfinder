//
//  CommandAcceptalbe.swift
//  FriendsFinder
//
//  Created by McSims on 01/12/2019.
//  Copyright © 2019 MP. All rights reserved.
//

import Foundation

protocol StreamAcceptable {
    init(with configuration: StreamConfig)
    func send(message: String)
    func closeConnection()
}
